defmodule Fibo do

  def fibs(length) do
    fibs = Stream.unfold({1, 1}, fn {a, b} ->
      {a, {b, a + b}}
    end)

    fibs
    |> Enum.take(length)
  end
end