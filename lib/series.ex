defmodule Series do

  #
  # Calculate sum, with specific to general cases, top to bottom
  # so that pattern matching handles the dispatching.
  #
  def sum(0), do: "0.00"
  def sum(length) when 0 > length, do: "0.00"
  def sum(length) do
    list = Enum.to_list 1..length
    sum = Enum.reduce(list, 0, fn(x, acc) -> acc + (1 / denom(x)) end)
    Float.to_string(sum, decimals: 2)
  end

  #
  # Stream based calculation of the denominators needed to calculate sum
  #
  def denom(0), do: 0
  def denom(1), do: 1
  def denom(length) do
    denoms = Stream.unfold({1,4}, fn {a, b} ->
      {a, {b, b + 3}}
    end)

    denoms
    |> Enum.take(length)
    |> Enum.at(-1)
  end
end