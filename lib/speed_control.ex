defmodule Speedcontrol do

  def gps(_s, []), do: 0
  def gps(_s, [_]), do: 0
  def gps(s, x) do
    Enum.chunk(x, 2, 1)
    |> Enum.map(fn([x, y]) -> y - x end)
    |> Enum.map(fn(d) -> 60 / s * 60 * d end)
    |> Enum.max
    |> Float.floor
  end
end