defmodule MaxballTest do
  use ExUnit.Case

  defp testing(numtest, v0, ans) do
    IO.puts("Test #{numtest}")
    assert Maxball.max_ball(v0) == ans
  end

  @tag :maxball
  test "max_ball" do
    testing(1, 37, 10)
    testing(2, 45, 13)
    testing(3, 99, 28)
    testing(4, 85, 24)
  end
end