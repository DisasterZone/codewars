import Fibo, only: [fibs: 1]

defmodule TestFibo do
  use ExUnit.Case

  @tag :fibs
  test "Fibo fibs" do
    assert fibs(1) == [1]
    assert fibs(2) == [1, 1]
    assert fibs(3) == [1, 1, 2]
    assert fibs(10) == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
  end
end