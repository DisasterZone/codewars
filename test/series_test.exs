import Series, only: [sum: 1, denom: 1]

defmodule TestSeries do
  use ExUnit.Case

  @tag :sum
  test "series sum" do
    assert sum(-1) == "0.00"
    assert sum(0) == "0.00"
    assert sum(1) == "1.00"
    assert sum(2) == "1.25"
    assert sum(3) == "1.39"
    assert sum(4) == "1.49"
    assert sum(5) == "1.57"
  end

  @tag :denom
  test "series denom" do
    assert denom(0) == 0
    assert denom(1) == 1
    assert denom(2) == 4
    assert denom(3) == 7
    assert denom(4) == 10
    assert denom(5) == 13
    assert denom(6) == 16
  end

end